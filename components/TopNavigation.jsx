import React            from 'react'
import { Router, Link } from 'react-router'

/**
 * Top navigation menu.
 * @param {string} active | information on menu state
 */
const TopNavigation = function({ active }) {
   let a, b, c;
   a = b = c = 'item default'

   switch (active) {
      case 'home'    : a = 'item active'; break
      case 'about'   : b = 'item active'; break
      case 'contact' : c = 'item active'; break
      default: throw new TypeError(`TopNavigation received invalid input.`)
   }

   return (
      <header>
         <nav className='nav'>
            <div className='left-logo'>IMEC</div>
            <div className='right-menu'>
               <Link to='/'        className={a}>Home</Link>
               <Link to='/about'   className={b}>About</Link>
               <Link to='/contact' className={c}>Contact</Link>
            </div>
         </nav>

         <hr />
      </header>
   )
}

export default TopNavigation