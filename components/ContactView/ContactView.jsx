import React from 'react'

import TopNavigation from '../TopNavigation.jsx'
import Articles      from './Articles.jsx'
import Footer        from '../Footer.jsx'

import '../../css/custom.scss'
import '../../css/ContactView.scss'

const ContactView = function() {
   return (
      <section id='ContactView'>
         <TopNavigation active='contact' />
         <Articles />
         <div className='empty' />
         <Footer />
      </section>
   )
}

export default ContactView