import React from "react"

const Articles = function() {
   return (
      <article>
         <h1>Contact us</h1>
         <section className="table">
            <div className="row">
               <label>Phone</label>
               <data>408 257 6676</data>
            </div>
            <div className="row">
               <label>Website</label>
               <data><a href="//mpmmath.us">mpmmath.us</a></data>
            </div>
            <div className="row">
               <label>Email</label>
               <data><a href="mailto:info@imecjunior.com">info@imecjunior.com</a></data>
            </div>
            <div className="row">
               <label className="address">Address</label>
               <data className="address">
                  MPM Educational Institute <span className="helper"><i className="fa fa-search"></i> <a href="https://goo.gl/BH7M1D" target="_blank">Map now</a></span> <br />
                  6176 Bollinger Road <br />
                  San Jose, CA 95129
               </data>
            </div>
         </section>
      </article>
   )
}

export default Articles