import React from 'react'
import { Link } from 'react-router'

const BodyContainer = function() {
   return (
      <section id='BodyContainer'>
         <article>
            <main>
               <h3>About IMEC</h3>
               <p className="copy">The IMEC is an annual competitive math event for K to 6th
                  grade students. The questions in the contest are carefully
                  designed for each grade level to generate interest in math for
                  students and to help parents acknowledge the progress of their
                  children's math development.</p>
               <p className="copy">The IMEC is designed to stimulate interest in mathematics for
                  young students through challenging problems, competitive
               experience, and rewards.</p>
               <Link to="/about" className='button'>
                  Learn more
                  <i className="fa fa-chevron-right"></i>
               </Link>
            </main>
            <aside>
               <img src='./images/thinking_girl.jpeg' />
            </aside>
         </article>

         <article>
            <main>
               <h3>About us</h3>
               <p className="copy">We are an organization of teachers, private educational
                  institutes, and after school programs dedicated to the
                  interest and development of math and science in young
               children.</p>
               <p className="copy">IMEC 2016 is organized by the International Math Teaching
                  Association and co-organized by MPM Educational Institute, and
                  is sponsored by World Journal, the Association of Northern
                  California Chinese Schools, and India West.</p>
               <Link to="/contact" className='button'>
                  Learn more
                  <i className="fa fa-chevron-right"></i>
               </Link>
            </main>
            <aside>
               <img src='./images/smiling_boy.jpeg' />
            </aside>
         </article>
      </section>
   )
}

export default BodyContainer
