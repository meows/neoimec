import React from 'react'

const Hero = function() {
   return (
      <section id='hero'>
         <hgroup>
            <h1 className='title'>International Math</h1>
            <h1 className="title">Evaluation Contest</h1>
         </hgroup>
         <hgroup>
            <h2 className='subtitle'>Welcome to the IMEC 2017</h2>
            <h2 className='subtitle'>Registration begins January 1st</h2>
         </hgroup>
         <a href='#' className='button'>
            Register now
            <i className="fa fa-chevron-right"></i>
         </a>
      </section>
   )
}

export default Hero