import React from 'react'

import TopNavigation  from '../TopNavigation.jsx'
import Hero           from './Hero.jsx'
import BodyContainer  from './BodyContainer.jsx'
import Footer         from '../Footer.jsx'

import '../../css/custom.scss'
import '../../css/AppView.scss'

const AppView = function() {
   return (
      <section id='AppView'>
         <TopNavigation active="home" />
         <Hero />
         <hr />
         <BodyContainer />
         <Footer />
      </section>
   )
}

export default AppView