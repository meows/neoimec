import React from 'react'

const Footer = function() {
   const email = 'info@imecjunior.com'
   return (
      <footer id='footer'>
         <hr />
         Contact us at <a href={`mailto:${email}`}>{email}</a>
      </footer>
   )
}

export default Footer