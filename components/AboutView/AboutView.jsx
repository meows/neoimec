import React         from 'react'
import TopNavigation from '../TopNavigation.jsx'
import Footer        from '../Footer.jsx'

import '../../css/custom.scss'
import '../../css/AboutView.scss'

const AboutView = function() {
   return (
      <section id="AboutView">
         <TopNavigation active="about" />
         <BodyContainer />
         <Footer />
      </section>
   )
}

const BodyContainer = function() {
   const year = '2016'  // imec year
   return (
      <section id="AboutBody">
         <article>
            <h2>About IMEC {year}</h2>
            <p className="copy">The IMEC is an annual competitive math event for
               K to 6th grade students. The questions in the contest are
               carefully designed for each grade level to generate interest and
               pride in math for students and to help parents acknowledge the
               progress of their children's math development.</p>
         </article>

         <article>
            <h2>Registration</h2>
            <ul className="copy">
               <li><a href="#">Register online here</a>.</li>
            </ul>
         </article>

         <article>
            <h2>Contest Format</h2>
            <ul className="copy">
               <li>The contest is open to kindergarten to 6th grade students.</li>
               <li>Students will have 60 minutes to work on 26 multiple choice
                  questions and 4 free response questions.</li>
               <li>Every student will receive a performance report 30 days after
                  the contest, and every student will receive a reward for
                  undertaking this challenge.</li>
               <li>The first, second, and third best contestants, along with
                  honorable mentions from each grade level, will be invited to
                  an award ceremony held at the Milpitas Community Center on
                  May 1, 2016.</li>
               <li>Entry fee of $30 is payable by check to MPM Educational
                  Institute. ($40 for walk-in registration at the Convention
                  Center on the test date.)</li>
               <li>A complimentary Lexile Test is offered to Grade 5 & 6 who
                  have signed up for the IMEC test. It is designed to test
                  students' English & Math abilities for preparing their future
                  plans for college.</li>
            </ul>
         </article>

         <article>
            <h2>Test Date</h2>
            <table className="ui very basic compact unstackable selectable table">
               <thead>
                  <tr>
                     <th>Student grade</th>
                     <th>Check-in time</th>
                     <th>Test time</th>
                  </tr>
               </thead>
               <tbody>
                  <tr>
                     <td>Kindergarten, 1st, 2nd, 3rd</td>
                     <td>12:20 pm – 12:50 pm</td>
                     <td>1:00 pm – 2:00 pm</td>
                  </tr>
                  <tr>
                     <td>4th, 5th, 6th</td>
                     <td>2:20 pm – 2:50 pm</td>
                     <td>3:00 pm – 4:00 pm</td>
                  </tr>
                  <tr>
                     <td>Lexile</td>
                     <td>4:10 pm – 4:20 pm</td>
                     <td>4:30 pm – 5:00 pm</td>
                  </tr>
               </tbody>
            </table>
         </article>

         <article>
            <h2>Test locations</h2>
            <div className="flexed">
               <address className="copy">
                  Great American Meeting Room <br />
                  Santa Clara Convention Center <br />
                  5001 Great America Parkway <br />
                  Santa Clara, CA 95045
               </address>
               <span className="helper"><i className="fa fa-search"></i> <a href="https://goo.gl/BH7M1D" target="_blank">Map now</a></span>
            </div>
         </article>
      </section>
   )
}

export default AboutView
