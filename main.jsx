import React    from 'react'
import ReactDOM from 'react-dom'
import { createHashHistory } from 'history'
import { Router, Route, Link, useRouterHistory } from 'react-router'

import AppView      from './components/AppView/AppView.jsx'
import AboutView    from './components/AboutView/AboutView.jsx'
import ContactView  from './components/ContactView/ContactView.jsx'

// -----------------------------------------------------------------------------
// Render

const render = function() {
   const appHistory = useRouterHistory(createHashHistory)({ queryKey: false })
   ReactDOM.render(
      <Router history={appHistory} onUpdate={() => window.scrollTo(0,0)}>
         <Route path='/'         component={AppView}     />
         <Route path='/about'    component={AboutView}   />
         <Route path='/contact'  component={ContactView} />
      </Router>,
      document.getElementById('app')
   )
}

render()

console.log(`\
---------------------------------------------

   Problem? Tell me at info@imecjunior.com

---------------------------------------------
`)